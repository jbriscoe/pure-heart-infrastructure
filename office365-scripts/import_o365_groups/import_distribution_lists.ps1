param(
        [Parameter(Mandatory)]
        [string]$file,
        [Parameter()]
        [System.Management.Automation.PSCredential]$Credential
    )
Write-Host 'Here we go!'

if($Credential -eq [System.Management.Automation.PSCredential]::Empty) {
    #Get Credentials to connect
    try {$Credential = Get-Credential }
    catch{
        Write-Host "Ah ah ah… you didn’t say the magic word”
        $ErrorRecord.InvocationInfo.PositionMessage
    } 
}

try {
#Create the session
#$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $Credential -Authentication Basic -AllowRedirection
  
#Import the session
#Import-PSSession $Session -DisableNameChecking
}
catch{
    Write-Host "Uh-oh. Couldn't connect to the Office API.”
    $ErrorRecord.InvocationInfo.PositionMessage
}

class DistributionListItem{
    DistributionListItem( ){ }
    DistributionListItem([string]$email_address,[string]$group_name,[string]$role_title,[string]$member_type){
        $this.email = $email_address
	$this.group = $group_name
	$this.role  = $role_title
    $this.type = $member_type	
        #Write-Host "Creating DistributionListItem $($this.email),$($this.group),$($this.role),$($this.type)"
    }
    [String] ToString(){
        $output = "Member:" +$this.email + ","
        $output += "Group:" +$this.group + ","
        $output += "Role:" +$this.role + ","
        $output += "Type:" +$this.type + "`n"
        return $output
    }
    [string]$email
    [string]$group
    [string]$role
    [string]$type
    	
}
$filepath = Resolve-Path $file
$memberList = New-Object -TypeName "System.Collections.ArrayList"
Import-Csv $filepath | ForEach-Object {
    #Write-Host "Group: $($_.group), Member: $($_.member), Role: $($_.role), Type: $($_.type)"
    $temp = New-Object -Typename DistributionListItem($($_.member),$($_.group),$($_.role),$($_.type))
    #Write-Host "Temp: $($temp)"
    $memberList.add($temp)   
    Write-Host "Imported: $($memberList.count)"
}

Write-host -f Green "What's in the memberList?"
write-host $memberList

$grouplist = $memberlist | Sort-Object -property group -unique | Select-Object -property group
## group, member, role, type

ForEach ($group in $grouplist) {
    $name = $group | select -expandproperty group
    $groupname_split = $name.split("@",2)
    $groupname = $groupname_split[0] 
    #New-UnifiedGroup -DisplayName $groupname -Alias $groupname -EmailAddresses $name -AccessType Private -AutoSubscribeNewMembers -RequireSenderAuthenticationEnabled $false
    Write-host -f Green "Added Group '$($name)' to Office 365 Group Email"
    Clear-Variable groupname_split
    Clear-Variable groupname
    Clear-Variable name
}

#Test For Each Group
#ForEach-Object -inputobject $grouplist {
 #    Write-host -f Green "Test Group: '$($_.group)'"
  #   $_.group
     #$_.group | select -expandproperty group
    # $member
 #}

#Test For Each Member
#ForEach($member in $memberlist) {
   # Write-host -f Green "Test Member: '$($member.email)'"
   # Write-host -f Green "Test Group: '$($member.Group)'"
   # $member
#}

ForEach($member in $memberlist) {
    if ($member.Role -eq "MEMBER" ){
        Add-UnifiedGroupLinks –Identity $member.group –LinkType Members –Links $member.email
        Write-host -f Green "Added Member '$($member.email)' to Office 365 Group '$($member.group)'"
    }
    elseif ($_.Role -eq "OWNER") {
        Add-UnifiedGroupLinks –Identity $member.group –LinkType Owners –Links $member.email
        Write-host -f Green "Added Owner '$($member.email)' to Office 365 Group '$($member.group)'"
    }
}
function Resolve-Error ($ErrorRecord=$Error[0])
{
   $ErrorRecord | Format-List * -Force
   $ErrorRecord.InvocationInfo |Format-List *
   $Exception = $ErrorRecord.Exception
   for ($i = 0; $Exception; $i++, ($Exception = $Exception.InnerException))
   {   "$i" * 80
       $Exception |Format-List * -Force
   }
}

